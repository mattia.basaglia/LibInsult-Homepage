LibInsult Homepage
==================

This repository contains the sources for the libinsult home page.


Intallation
-----------

Install insult data and library.
Then create the file ./project/settings_local.py and set INSULT_WORD_PATH.


License
-------

GPLv3+, see COPYING


Sources
-------

See http://insult.mattbas.org


Author
------

Mattia Basaglia <mattia.basaglia@gmail.com>

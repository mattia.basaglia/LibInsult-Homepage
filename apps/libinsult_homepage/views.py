#
# Copyright (C) 2016-2022 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.shortcuts import render
from django.template import loader
from django.core.urlresolvers import reverse
import six


class Link(object):
    def __init__(self, url, text):
        self.url = url
        self.text = text


class LinkGroup(object):
    def __init__(self, title="", links=[]):
        self.title = title
        self.links = links

    def add_link(self, link):
        self.links.append(link)


class Page(object):
    site_name = "LibInsult"
    template_root = "libinsult_homepage/"
    base_template = template_root + "base.html"

    def __init__(self, title, contents_template, contents_context={}):
        self.title = title
        self.breadcrumbs = LinkGroup()
        self.footer = [
            LinkGroup("Sources", [
                Link("https://gitlab.com/mattia.basaglia/LibInsult-Data", "Data"),
                Link("https://gitlab.com/mattia.basaglia/LibInsult-Python", "Python"),
                Link("https://gitlab.com/mattia.basaglia/LibInsult-Django", "API (Python + Django)"),
            ]),
            LinkGroup("API", [
                Link(reverse("insult:help"), "Documentation"),
                Link(reverse("insult:explore"), "Explore"),
            ]),
        ]
        self.contents_template = self.template_root + contents_template
        self.contents_context = contents_context

    def context(self):
        ctx = self._obj_to_dict(Page)
        ctx.update(self._obj_to_dict(self))
        ctx["contents"] = loader.get_template(self.contents_template).render(self.contents_context)
        return ctx

    def render(self, request, status_code=None):
        return render(request, self.base_template, self.context(), status_code)

    def _obj_to_dict(self, object):
        return {
            name: val
            for name, val in six.iteritems(vars(object))
            if not name.startswith("_")
        }


def home(request):
    page = Page("LibInsult", "home.html")
    return page.render(request)
